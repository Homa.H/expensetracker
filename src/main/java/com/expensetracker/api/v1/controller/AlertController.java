package com.expensetracker.api.v1.controller;


import com.expensetracker.api.v1.domain.AlertDto;
import com.expensetracker.api.v1.service.AlertService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/alert")
@Api(value = "Expense Tracker API. Set of endpoints for Alert Operation for Expenses tracker.")
public class AlertController {
    private static final Logger logger = LoggerFactory.getLogger(AlertController.class);

    private final AlertService alertService;
    AlertController(AlertService alertService){
        this.alertService = alertService;
    }


    @PostMapping("/create")
    @ApiOperation(value = "Endpoint for create  Alert. It returns a Alert object due to creating new Alert.",
            response = AlertDto.class, httpMethod = "POST")
    ResponseEntity<AlertDto> createAlert(@RequestBody @Valid AlertDto newAlert){
        logger.info("Create Alert");
        return new ResponseEntity<>(alertService.createAlert(newAlert),HttpStatus.CREATED);
    }

    @ApiOperation(value = "Endpoint for Get Alert. It returns list of Alerts based on User.",
            response = List.class, httpMethod = "GET")
    @GetMapping("/get")
    ResponseEntity<List<AlertDto>> getAlerts(){
        return new ResponseEntity<>(alertService.getAlerts(), HttpStatus.OK);

    }

    @GetMapping("/get/{categoryId}")
    @ApiOperation(value = "Endpoint for get  Alert. It returns a Alert object based on category id.",
            response = AlertDto.class, httpMethod = "GET")
    ResponseEntity<AlertDto> getAlert(@PathVariable Long categoryId){
        return new ResponseEntity<>(alertService.getAlert(categoryId),HttpStatus.OK);
    }

    @PutMapping("/update")
    @ApiOperation(value = "Endpoint for edit/update Alert. It returns a Alert updated.",
            response = AlertDto.class, httpMethod = "PUT")
    ResponseEntity<AlertDto> updateAlert(@RequestBody @Valid AlertDto alert){
        return new ResponseEntity<>(alertService.updateAlert(alert),HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "Endpoint for delete Alert. It returns Status due to deletion successfully.",
            response = void.class, httpMethod = "DELETE")
    ResponseEntity<HttpStatus> deleteAlert(@PathVariable @Valid Long id){
        alertService.deleteAlert(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }


}
