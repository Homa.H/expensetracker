package com.expensetracker.api.v1.service.mapper;


import com.expensetracker.api.v1.domain.IncomeDto;
import com.expensetracker.api.v1.entity.Income;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IncomeMapper extends EntityMapper<IncomeDto, Income> {


    @Mapping(target = "id", source = "income.id")
    @Mapping(target = "label", source = "income.label")
    @Mapping(target = "amount", source = "income.amount")
    @Mapping(target = "date", source = "income.date")
    @Mapping(target = "description", source = "income.description")
    @Mapping(target = "categoryId", expression = "java(Long.valueOf(income.getCategory().getId()))")
    IncomeDto toDto(Income income);

    List<IncomeDto> toDto(List<Income> income);


    @Mapping(target = "id", source = "dto.id")
    @Mapping(target = "label", source = "dto.label")
    @Mapping(target = "amount", source = "dto.amount")
    @Mapping(target = "date", source = "dto.date")
    @Mapping(target = "description", source = "dto.description")
    Income toEntity(IncomeDto dto);

    List<Income> toEntity(List<IncomeDto> dto);
}
