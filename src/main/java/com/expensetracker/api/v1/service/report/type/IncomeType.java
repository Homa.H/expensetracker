package com.expensetracker.api.v1.service.report.type;

import com.expensetracker.api.v1.entity.Income;
import com.expensetracker.api.v1.enums.TypeEnum;
import com.expensetracker.api.v1.repository.IncomeRepository;
import com.expensetracker.api.v1.service.mapper.IncomeMapper;
import com.expensetracker.api.v1.service.report.Type;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class IncomeType implements Type {
    private final IncomeRepository incomeRepository;
    private final IncomeMapper incomeMapper;

    @Override
    public Object getData(Date start, Date end) {
        List<Income> report = incomeRepository.findAllByDateBetween(start,end);
        return incomeMapper.toDto(report);
    }

    @Override
    public TypeEnum getStrategyReport() {
        return TypeEnum.INCOME;
    }
}
