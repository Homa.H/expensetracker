package com.expensetracker.api.v1.domain.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class GetIncomeRequest {

    @NotNull
    @NotBlank
    private String startDate;
    @NotNull
    @NotBlank
    private String endDate;
    private Long categoryId;
}
