package com.expensetracker.api.v1.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class BaseController {

    @GetMapping("/start")
    @ResponseBody String start(){
        return "This is Expense Tracker App";
    }

}
