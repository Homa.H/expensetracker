package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.BalanceDto;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.repository.BalanceRepository;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.service.mapper.BalanceMapper;
import com.expensetracker.api.v1.entity.Balance;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.security.SecurityUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;



@Service
@Transactional
public class BalanceServiceImpl implements BalanceService{

    private final BalanceRepository balanceRepository;
    private final UserRepository userRepository;
    private final BalanceMapper balanceMapper;
    BalanceServiceImpl(BalanceRepository balanceRepository,
                       UserRepository userRepository,
                       BalanceMapper balanceMapper){
        this.balanceRepository = balanceRepository;
        this.userRepository = userRepository;
        this.balanceMapper = balanceMapper;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public BalanceDto getBalance() {
        Balance balance =  balanceRepository.findBalanceByUser(getUser()).get();
        return balanceMapper.toDto(balance);
    }

    @Override
    public BalanceDto addExpense(Double amount) {
        Optional<Balance> existedBalance = balanceRepository.findBalanceByUser(getUser());
        if(existedBalance.isPresent()){
            return updateExpenseOfBalance(existedBalance.get(),amount);
        }
        Balance newBalance = Balance.builder().totalExpense(amount).totalIncome(0.0).balance(-(amount)).build();
        return balanceMapper.toDto(balanceRepository.save(newBalance));

    }

    @Override
    public BalanceDto addIncome(Double amount) {
        Optional<Balance> existedBalance = balanceRepository.findBalanceByUser(getUser());
        if(existedBalance.isPresent()){
            return updateIncomeOfBalance(existedBalance.get(),amount);
        }
        Balance newBalance = Balance.builder().totalIncome(amount).totalExpense(0.0).balance(-(amount)).build();
        return balanceMapper.toDto(balanceRepository.save(newBalance));
    }

    private User getUser() {
        String username = SecurityUtils.getCurrentUser();
        return userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("USER_NOT_FOUND_EXCEPTION"));
    }

    private BalanceDto updateIncomeOfBalance(Balance existedBalance,Double amount) {
        existedBalance.setTotalIncome(amount+existedBalance.getTotalIncome());
        existedBalance.setBalance(existedBalance.getBalance()+amount);
        return balanceMapper.toDto(balanceRepository.save(existedBalance));
    }
    private BalanceDto updateExpenseOfBalance(Balance existedBalance, Double amount) {
        existedBalance.setTotalExpense(amount+existedBalance.getTotalExpense());
        existedBalance.setBalance(existedBalance.getBalance()-amount);
        return balanceMapper.toDto(balanceRepository.save(existedBalance));
    }
}
