package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.AlertMessage;
import com.expensetracker.api.v1.domain.BalanceDto;
import com.expensetracker.api.v1.domain.ExpenseDto;
import com.expensetracker.api.v1.entity.Alert;
import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.Expense;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.repository.AlertRepository;
import com.expensetracker.api.v1.repository.CategoryRepository;
import com.expensetracker.api.v1.repository.ExpenseRepository;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.service.BalanceService;
import com.expensetracker.api.v1.service.ExpenseServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;


@SpringBootTest
class ExpenseServiceImplTest {

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();

    @MockBean
    ExpenseRepository expenseRepositoryMock;

    @MockBean
    CategoryRepository categoryRepositoryMock;

    @MockBean
    BalanceService balanceService;

    @MockBean
    AlertRepository alertRepositoryMock;

    @MockBean
    UserRepository userRepositoryMock;

    @InjectMocks
    @Autowired
    ExpenseServiceImpl expenseServiceMock;

    User user;
    Expense expenseEntity;
    ExpenseDto expense;
    Alert alert;
    String url;
    AlertMessage alertMessage;
    BalanceDto balanceDto;
    Category category;

    @BeforeEach
    public void init() throws ParseException {
        String start = "2022-03-04 11:30:00";
        String end = "2022-03-05 11:30:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startDate = LocalDateTime.parse(start, formatter);
        LocalDateTime endDate = LocalDateTime.parse(end, formatter);


        mockServer = MockRestServiceServer.createServer(restTemplate);
        user = User.builder().id(1L).username("user").password("password").email("user@email.com").build();
        expense = ExpenseDto.builder().categoryId(12L).label("car_expense")
                .amount(1000.0).date(start).description("this is car expense").build();
        category = new Category();
        category.setDescription("this is car category");
        category.setName("car");
        category.setId(12L);
        expenseEntity = Expense.builder().id(1L).category(category).user(user).amount(1000.0)
                        .description("this is car expense").build();
        alert = Alert.builder().startTime(startDate)
                .endTime(endDate)
                .description("this is alert")
                .balance(60000.0)
                .category(category)
                .user(user)
                .build();
        alertMessage = new AlertMessage();
        alertMessage.setMessage(alert.getDescription());
        balanceDto = BalanceDto.builder().id(12L).totalExpense(2000.0).totalIncome(4000.0).balance(2000.0).build();
        url = "http://localhost:8080/produce/message";
    }

    @Test
    @WithMockUser
    void given_newExpense_when_createNewExpense_then_saveAndCreateNewExpenseWithSendMessageForAboveBalance() throws URISyntaxException, JsonProcessingException {
        //given
        //expense
        alert.setBalance(500.0);
        //when
        Mockito.when(categoryRepositoryMock.findById(expense.getCategoryId())).thenReturn(Optional.of(category));
        Mockito.when(alertRepositoryMock.findAllByUserAndCategory(any(User.class),any(Category.class))).thenReturn(Optional.of(alert));
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(expenseRepositoryMock.sumOfExpense(any(LocalDateTime.class),any(LocalDateTime.class),any(Category.class))).thenReturn(2000.0);
//        Mockito.when(restTemplate.postForEntity(url,alertMessage,AlertMessage.class))
//          .thenReturn(new ResponseEntity(alertMessage, HttpStatus.OK));
        mockServer.expect(ExpectedCount.once(),
                        requestTo(new URI(url)))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(alertMessage)));
        Mockito.when(expenseRepositoryMock.save(any(Expense.class))).thenReturn(expenseEntity);
        Mockito.when(balanceService.addExpense(any(Double.class))).thenReturn(balanceDto);
        ExpenseDto newExpense = expenseServiceMock.createNewExpense(expense);

        //then
        assertEquals(newExpense.getAmount(), expense.getAmount());
        assertEquals(newExpense.getDescription(), expense.getDescription());



    }

    @Test
    @WithMockUser
    void given_newExpense_when_createNewExpense_then_saveAndCreateNewExpenseWithoutSendMessageForAboveBalance() throws JsonProcessingException, URISyntaxException {

        //given
        //expense instance

        Mockito.when(categoryRepositoryMock.findById(expense.getCategoryId())).thenReturn(Optional.of(category));
        Mockito.when(alertRepositoryMock.findAllByUserAndCategory(any(User.class),any(Category.class))).thenReturn(Optional.of(alert));
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(expenseRepositoryMock.sumOfExpense(any(LocalDateTime.class),any(LocalDateTime.class),any(Category.class))).thenReturn(2000.0);
        Mockito.when(expenseRepositoryMock.save(any(Expense.class))).thenReturn(expenseEntity);
        Mockito.when(balanceService.addExpense(any(Double.class))).thenReturn(balanceDto);
        ExpenseDto newExpense = expenseServiceMock.createNewExpense(expense);

        //then
        assertEquals(newExpense.getAmount(), expense.getAmount());
        assertEquals(newExpense.getDescription(), expense.getDescription());
    }


}