package com.expensetracker.api.v1.enums;

public enum DurationEnum {

    MONTHLY("monthly"),
    WEEKLY("weekly"),
    DAILY("daily");


    public final String strategyLabel;

    DurationEnum(String strategyLabel){
        this.strategyLabel = strategyLabel;
    }

    public static DurationEnum valueOfLabel(String label) {
        for (DurationEnum e : values()) {
            if (e.strategyLabel.equals(label)) {
                return e;
            }
        }
        return null;
    }


}