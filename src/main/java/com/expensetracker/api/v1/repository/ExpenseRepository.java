package com.expensetracker.api.v1.repository;

import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.Expense;
import com.expensetracker.api.v1.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Repository
public interface ExpenseRepository extends CrudRepository<Expense,Long> {

    List<Expense> findAllByDateBetweenAndCategoryAndUser(LocalDateTime startDate, LocalDateTime endDate, Category category, User user);
    List<Expense> findAllByDateBetweenAndUser(LocalDateTime startDate, LocalDateTime endDate,User user);
    Optional<Expense> findById(Long id);
    List<Expense> findAllByDateBetween(Date startDate, Date endDate);

    void deleteById(Long id);

    @Query(nativeQuery = true,value = "select sum(amount) from expense where date >= ? and date <= ? and category = ?")
    Double sumOfExpense(LocalDateTime startTime, LocalDateTime endTime, Category category);


}
