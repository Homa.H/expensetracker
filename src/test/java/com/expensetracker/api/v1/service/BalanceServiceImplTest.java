package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.BalanceDto;
import com.expensetracker.api.v1.entity.Balance;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.repository.BalanceRepository;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.service.mapper.BalanceMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
class BalanceServiceImplTest {

    @MockBean
    BalanceRepository balanceRepositoryMock;
    @MockBean
    @Autowired
    UserRepository userRepositoryMock;

    @Autowired
    @InjectMocks
    BalanceServiceImpl balanceService;

    @Autowired
    BalanceMapper balanceMapper;

    User user;

    @BeforeEach
    public void init() {
        user = new User();
        user.setUsername("my user");
        user.setPassword("123456");
        user.setId(1L);
        user.setEmail("email@gmail.com");
    }
    @Test
    @WithMockUser
    void given_nothing_when_getBalance_then_returnBalanceEntityObject() {
        BalanceDto balanceDto = BalanceDto.builder().id(12L).totalExpense(2000.0).totalIncome(4000.0).balance(2000.0).build();
        Balance balance = balanceMapper.toEntity(balanceDto);

        //when
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(balanceRepositoryMock.findBalanceByUser(any(User.class))).thenReturn(Optional.of(balance));
        BalanceDto getBalanceDto = balanceService.getBalance();

        //then
        assertEquals(getBalanceDto.getBalance(), balanceDto.getBalance());
        assertEquals(getBalanceDto.getTotalExpense(), balanceDto.getTotalExpense());
        assertEquals(getBalanceDto.getTotalIncome(),balanceDto.getTotalIncome());

    }

    @Test
    @WithMockUser
    void given_amountOfExpense_when_addExpense_then_updateBalance() {
        //given
        Double expense = 200.0;

        Balance balance = new Balance();
                balance.setTotalExpense(4000.0);
                balance.setTotalIncome(6000.0);
                balance.setBalance(2000.0);
        Balance updatedBalance = new Balance();
        updatedBalance.setTotalExpense(4200.0);
                updatedBalance.setTotalIncome(6000.0);
                updatedBalance.setBalance(1800.0);

        //when
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(balanceRepositoryMock.findBalanceByUser(any(User.class))).thenReturn(Optional.of(balance));
        Mockito.when(balanceRepositoryMock.save(balance)).thenReturn(updatedBalance);
        BalanceDto getUpdatedBalance = balanceService.addExpense(expense);

        //then
        assertEquals(getUpdatedBalance.getBalance(), 1800.0);
        assertEquals(getUpdatedBalance.getTotalExpense(), 4200.0);
        assertEquals(getUpdatedBalance.getTotalIncome(),6000.0);
    }

    @Test
    @WithMockUser
    void given_amountOfIncome_when_addIncome_then_updateBalance() {

        //given
        Double income = 200.0;
        Balance balance = new Balance();
        balance.setTotalExpense(4000.0);
        balance.setTotalIncome(6000.0);
        balance.setBalance(2000.0);
        Balance updatedBalance = new Balance();
        updatedBalance.setTotalExpense(4200.0);
        updatedBalance.setTotalIncome(6200.0);
        updatedBalance.setBalance(2200.0);

        //when
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(balanceRepositoryMock.findBalanceByUser(any(User.class))).thenReturn(Optional.of(balance));
        Mockito.when(balanceRepositoryMock.save(balance)).thenReturn(updatedBalance);
        BalanceDto getUpdatedBalance = balanceService.addIncome(income);

        //then
        assertEquals(getUpdatedBalance.getBalance(), updatedBalance.getBalance());
        assertEquals(getUpdatedBalance.getTotalExpense(), updatedBalance.getTotalExpense());
        assertEquals(getUpdatedBalance.getTotalIncome(),updatedBalance.getTotalIncome());
    }

    @Test
    @WithMockUser
    void given_amountOfIncome_when_addIncome_then_throwException(){
        //given
        Double income = 200.0;

        //when
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(balanceRepositoryMock.findBalanceByUser(any(User.class))).thenThrow(new ResourceNotFoundException("Balance related to the current user NOT Found"));
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            balanceService.addIncome(income);
        });

        String getMessage = exception.getMessage();
        String expectedMessage = "Balance related to the current user NOT Found";
        //then
        assertEquals(getMessage,expectedMessage);

    }

    @Test
    @WithMockUser
    void given_amountOfExpense_when_addExpense_then_throwException(){
        //given
        Double expense = 200.0;

        //when
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(balanceRepositoryMock.findBalanceByUser(any(User.class))).thenThrow(new ResourceNotFoundException("Balance related to the current user NOT Found"));
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            balanceService.addExpense(expense);
        });

        String getMessage = exception.getMessage();
        String expectedMessage = "Balance related to the current user NOT Found";
        //then
        assertEquals(getMessage,expectedMessage);

    }


}