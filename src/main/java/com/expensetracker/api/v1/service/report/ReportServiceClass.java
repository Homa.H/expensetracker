package com.expensetracker.api.v1.service.report;



import java.util.Date;

public class ReportServiceClass {

    private final Duration duration;
    private final Type type;

    public ReportServiceClass(Duration duration, Type type) {
        this.duration = duration;
        this.type = type;
    }

    public Object getReport(){
        Date[] duration = this.duration.getDuration();
        Object data = type.getData(duration[0], duration[1]);
        return data;

    }

}
