package com.expensetracker.api.v1.service.report;

import com.expensetracker.api.v1.enums.DurationEnum;

import java.util.Date;


public interface Duration {

    Date[] getDuration();

    DurationEnum getStrategyReport();

}
