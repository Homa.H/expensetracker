package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.CategoryDto;
import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.repository.CategoryRepository;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.service.CategoryServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Optional;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
class CategoryServiceImplTest {


    @MockBean
    CategoryRepository categoryRepositoryMock;

    @MockBean
    UserRepository userRepositoryMock;

    @InjectMocks
    @Autowired
    CategoryServiceImpl categoryService;


    private Category category = new Category();
    private User user = new User();


    @BeforeEach
    public void init(){

        user.setUsername("my user");
        user.setPassword("123456");
        user.setId(1L);
        user.setEmail("email@gmail.com");


        category.setId(1L);
        category.setName("category-name");
        category.setDescription("category-description");
        category.setUser(user);

    }

    @Test
    @WithMockUser
    void  given_newCategory_when_createCategory_then_returnNewCategoryInstance() {
        {

            //Given
            CategoryDto categoryRequest = CategoryDto.builder().name("category-name").description("category-description").build();

            //When
            Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
            Mockito.when(categoryRepositoryMock.save(any(Category.class))).thenReturn(category);
            CategoryDto categoryDto = categoryService.createCategory(categoryRequest);

            //Then
            assertEquals(categoryDto.getName(), "category-name");
            assertEquals(categoryDto.getDescription(), "category-description");
            assertEquals(categoryDto.getId(),1L);

        }
    }

    @Test
    @WithMockUser
    void given_categoryId_when_getCategory_then_returnCategoryInstance(){

        //Given
        Long categoryId = 1L;

        //When
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(categoryRepositoryMock.findByIdAndUser(categoryId,user)).thenReturn(Optional.of(category));
        CategoryDto categoryDto = categoryService.getCategory(categoryId);

        //Then
        assertEquals(categoryDto.getName(), "category-name");
        assertEquals(categoryDto.getDescription(), "category-description");
        assertEquals(categoryDto.getId(),1L);

    }

    @Test
    @WithMockUser
    void given_incorrectCategoryId_when_getCategory_then_throwException(){

        //Given
        Long categoryId = 1L;

        //When
        Mockito.when(userRepositoryMock.findByUsername(any(String.class))).thenReturn(Optional.of(user));
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            categoryService.getCategory(categoryId);
        });

        String getMessage = exception.getMessage();
        String expectedMessage = "Not found Category with id = " +categoryId;
        //then
        assertEquals(getMessage,expectedMessage);
    }



    @Test
    @WithMockUser
    void given_newCategoryInstance_when_updateCategory_then_returnCategoryUpdated() {

        //Given
        CategoryDto categoryRequest = CategoryDto.builder().id(1L).name("category-name").description("category-description").build();

        //When
        Mockito.when(categoryRepositoryMock.findById(categoryRequest.getId())).thenReturn(Optional.of(category));
        Mockito.when(categoryRepositoryMock.save(category)).thenReturn(category);
        CategoryDto categoryDto = categoryService.updateCategory(categoryRequest);

        //Then
        assertEquals(categoryDto.getName(), "category-name");
        assertEquals(categoryDto.getDescription(), "category-description");
        assertEquals(categoryDto.getId(),1L);

    }

}