package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.CategoryDto;

import java.util.List;


public interface CategoryService {

    CategoryDto createCategory(CategoryDto newCategory);
    List<CategoryDto> getCategories();
    CategoryDto getCategory(Long categoryId);
    CategoryDto updateCategory(CategoryDto category);
    void deleteCategory(Long categoryId);

}
