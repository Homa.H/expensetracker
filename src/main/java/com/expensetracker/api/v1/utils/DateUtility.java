package com.expensetracker.api.v1.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtility {

    static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.PATTERN_DATE);
    public static String localDateTimeToString(LocalDateTime localDateTime){
        return localDateTime.format(formatter);
    }
    public static LocalDateTime stringToLocalDateTime(String date){return LocalDateTime.parse(date, formatter);

    }

}
