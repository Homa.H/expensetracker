package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.ExpenseDto;
import com.expensetracker.api.v1.domain.request.GetExpenseRequest;

import java.util.List;

public interface ExpenseService {

    ExpenseDto createNewExpense(ExpenseDto newExpense);
    ExpenseDto updateExpense(ExpenseDto expense);
    void deleteExpense(Long id);
    List<ExpenseDto> getExpenses(GetExpenseRequest getExpenseRequest);

}
