package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.IncomeDto;
import com.expensetracker.api.v1.domain.request.GetIncomeRequest;
import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.Income;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.repository.CategoryRepository;
import com.expensetracker.api.v1.repository.IncomeRepository;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.security.SecurityUtils;
import com.expensetracker.api.v1.service.mapper.IncomeMapper;
import com.expensetracker.api.v1.utils.DateUtility;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;


@Service
@Transactional
public class IncomeServiceImpl implements IncomeService {

    private final UserRepository userRepository;
    private final IncomeMapper incomeMapper;
    private final IncomeRepository incomeRepository;
    private final CategoryRepository categoryRepository;
    private final BalanceService balanceService;

    public IncomeServiceImpl(UserRepository userRepository, IncomeMapper incomeMapper, IncomeRepository incomeRepository, CategoryRepository categoryRepository, BalanceService balanceService) {
        this.userRepository = userRepository;
        this.incomeMapper = incomeMapper;
        this.incomeRepository = incomeRepository;
        this.categoryRepository = categoryRepository;
        this.balanceService = balanceService;
    }

    @Override
    public IncomeDto createIncome(IncomeDto newIncome) {

        if (newIncome.getDate() == null) {
            LocalDateTime currentDateTime = LocalDateTime.now();
            newIncome.setDate(DateUtility.localDateTimeToString(currentDateTime));
        }
        Category category = categoryRepository.findById(newIncome.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("Not found category with id = " + newIncome.getCategoryId()));
        IncomeDto generatedIncome = saveIncome(newIncome, category);
        balanceService.addIncome(generatedIncome.getAmount());
        return generatedIncome;
    }

    @Override
    public IncomeDto updateIncome(IncomeDto income) {
        Category category = categoryRepository.findById(income.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("Not found category with id = " + income.getCategoryId()));
        Income existenceIncome = incomeRepository.findById(income.getId()).orElseThrow(() -> new ResourceNotFoundException("Not found income with id = " + income.getId()));
        return updateExistenceIncome(existenceIncome, income, category);
    }

    private IncomeDto updateExistenceIncome(Income existenceIncome, IncomeDto income, Category category) {
        checkUpdateBalance(existenceIncome, income);
        existenceIncome.setAmount(income.getAmount());
        existenceIncome.setDescription(income.getDescription());
        existenceIncome.setUser(getUser());
        existenceIncome.setCategory(category);
        existenceIncome.setDate(DateUtility.stringToLocalDateTime(income.getDate()));
        return incomeMapper.toDto(incomeRepository.save(existenceIncome));
    }

    private void checkUpdateBalance(Income existenceIncome, IncomeDto income) {
        if (!existenceIncome.getAmount().equals(income.getAmount())) {
            balanceService.addIncome(income.getAmount() - existenceIncome.getAmount());
        }
    }

    @Override
    public void deleteIncome(Long incomeId) {
        incomeRepository.deleteById(incomeId);
    }

    @Override
    public List<IncomeDto> getIncomes(GetIncomeRequest getIncomeRequest) {

        Timestamp startDate = Timestamp.valueOf(getIncomeRequest.getStartDate());
        Timestamp endDate = Timestamp.valueOf(getIncomeRequest.getEndDate());
        User user = getUser();

        if (getIncomeRequest.getCategoryId() != null) {

            Category category = categoryRepository.findById(getIncomeRequest.getCategoryId()).orElseThrow(EntityNotFoundException::new);
            List<Income> incomes = incomeRepository.findAllByDateBetweenAndCategoryAndUser(startDate, endDate, category, user);
            return incomeMapper.toDto(incomes);

        }
        List<Income> incomes = incomeRepository.findAllByDateBetweenAndUser(startDate, endDate, user);
        return incomeMapper.toDto(incomes);
    }

    private User getUser() {
        String username = SecurityUtils.getCurrentUser();
        return userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("USER_NOT_FOUND_EXCEPTION"));
    }

    private IncomeDto saveIncome(IncomeDto newIncome, Category category) {

        Income income = incomeMapper.toEntity(newIncome);
        income.setUser(getUser());
        income.setCategory(category);
        Income generatedIncome = incomeRepository.save(income);
        return incomeMapper.toDto(generatedIncome);
    }
}
