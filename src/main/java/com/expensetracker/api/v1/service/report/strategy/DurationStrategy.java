package com.expensetracker.api.v1.service.report.strategy;

import com.expensetracker.api.v1.service.report.Duration;
import com.expensetracker.api.v1.enums.DurationEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class DurationStrategy {
    private EnumMap<DurationEnum, Duration> strategies;

    @Autowired
    public DurationStrategy(Set<Duration> strategySet) {
        createStrategy(strategySet);
    }

    public Duration findStrategy(DurationEnum strategyName) {
        return strategies.get(strategyName);
    }


    private void createStrategy(Set<Duration> strategySet) {
        strategies = new EnumMap<>(DurationEnum.class);
        strategySet.forEach(
                strategy ->strategies.put(strategy.getStrategyReport(), strategy));
    }
}
