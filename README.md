# what is ExpenseTracker?

An expense tracker is a software or application that helps to keep an accurate record of your money inflow and outflow.



## Description
Users can log in to the system with a Username and Password then they create a Category for their expenses and incomes.
whenever they want to add expenses or incomes they need to select a category related to them. with adding expense or income, the balance of the system will be updated.
Users can get the report from the system based on expense or income. Users define an alert for a particular category and whenever the expense for that category is greater than the maximum that the user has defined then a message is displayed to the user.
## Techlogoies

```bash
java 8
Spring Boot
Spring JPA/Hibernate
Spring Security/JWT
ActiveMQ
Swagger
Junit
Mockito
Caching(In-Memory)
```
