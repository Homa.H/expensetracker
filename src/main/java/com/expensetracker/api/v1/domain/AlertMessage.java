package com.expensetracker.api.v1.domain;

import lombok.Data;

@Data
public class AlertMessage {

    private String message;

}
