package com.expensetracker.api.v1.controller;

import com.expensetracker.api.v1.domain.IncomeDto;
import com.expensetracker.api.v1.domain.request.GetIncomeRequest;
import com.expensetracker.api.v1.service.IncomeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/income")
@CrossOrigin
@Api(value = "Expense Tracker API. Set of endpoints for income Operation(create/update/get/delete) for Expenses tracker.")

public class IncomeController{
    private static final Logger logger = LoggerFactory.getLogger(ExpenseController.class);

    private final IncomeService incomeService;
    IncomeController(IncomeService incomeService){
        this.incomeService = incomeService;
    }


    @PostMapping("/create")
    @ApiOperation(value = "Endpoint for create  income. It returns a income object.",
            response = IncomeDto.class, httpMethod = "POST")
    ResponseEntity<IncomeDto> createIncome(@RequestBody @Valid IncomeDto newIncome){
        logger.info("create new income.");
        return new ResponseEntity<>(incomeService.createIncome(newIncome), HttpStatus.CREATED);
    }

    @PostMapping("/get")
    @ApiOperation(value = "Endpoint for Get Income. It returns a Income object based on time duration.",
            response = IncomeDto.class, httpMethod = "GET")
    ResponseEntity<List<IncomeDto>> getExpenseByDate(@RequestBody GetIncomeRequest getIncomeRequest){
        return new ResponseEntity<>(incomeService.getIncomes(getIncomeRequest),HttpStatus.OK);
    }
    @PutMapping("/update")
    @ApiOperation(value = "Endpoint for Edit/Update Income. It returns a Income object updated.",
            response = IncomeDto.class, httpMethod = "PUT")
    ResponseEntity<IncomeDto> updateIncome(@RequestBody @Valid IncomeDto income){
        return new ResponseEntity<>(incomeService.updateIncome(income),HttpStatus.OK);
    }

    @DeleteMapping("/delete/{incomeId}")
    @ApiOperation(value = "Endpoint for Delete Income.",
            response = HttpStatus.class, httpMethod = "DELETE")
    ResponseEntity<HttpStatus> deleteIncome(@PathVariable Long incomeId){
        incomeService.deleteIncome(incomeId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }



}
