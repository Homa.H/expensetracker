package com.expensetracker.api.v1.repository;

import com.expensetracker.api.v1.entity.Alert;
import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AlertRepository extends JpaRepository<Alert,Long> {

    List<Alert> findAllByUser(User user);
    Optional<Alert> findAllByUserAndCategory(User user,Category category);
}
