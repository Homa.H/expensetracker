package com.expensetracker.api.v1.service.report.type;

import com.expensetracker.api.v1.entity.Expense;
import com.expensetracker.api.v1.enums.TypeEnum;
import com.expensetracker.api.v1.repository.ExpenseRepository;
import com.expensetracker.api.v1.service.mapper.ExpenseMapper;
import com.expensetracker.api.v1.service.report.Type;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ExpenseType implements Type {

    private final ExpenseRepository expenseRepository;
    private final ExpenseMapper expenseMapper;

    @Override
    public Object getData(Date start, Date end) {
        List<Expense> dailyReport = expenseRepository.findAllByDateBetween(start,end);
        return expenseMapper.toDto(dailyReport);
    }

    @Override
    public TypeEnum getStrategyReport() {
        return TypeEnum.EXPENSE;
    }
}
