package com.expensetracker.api.v1.repository;

import com.expensetracker.api.v1.entity.Income;
import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface IncomeRepository extends JpaRepository<Income,Long> {
    List<Income> findAllByDateBetweenAndCategoryAndUser(Timestamp startDate, Timestamp endDate, Category category, User user);
    List<Income> findAllByDateBetween(Date startDate, Date endDate);

    List<Income> findAllByDateBetweenAndUser(Date startDate, Date endDate,User user);

}
