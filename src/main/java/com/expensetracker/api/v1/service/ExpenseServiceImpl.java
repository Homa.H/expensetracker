package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.AlertMessage;
import com.expensetracker.api.v1.domain.ExpenseDto;
import com.expensetracker.api.v1.domain.request.GetExpenseRequest;
import com.expensetracker.api.v1.entity.Alert;
import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.Expense;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.repository.AlertRepository;
import com.expensetracker.api.v1.repository.CategoryRepository;
import com.expensetracker.api.v1.repository.ExpenseRepository;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.security.SecurityUtils;
import com.expensetracker.api.v1.service.mapper.ExpenseMapper;
import com.expensetracker.api.v1.utils.DateUtility;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ExpenseServiceImpl implements ExpenseService {

    private final ExpenseRepository expenseRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final ExpenseMapper expenseMapper;
    private final AlertRepository alertRepository;
    private final BalanceService balanceService;
    private final RestTemplate restTemplate;


    @Value("${activemq.queue.url.path}")
    private String urlPath;

    public ExpenseServiceImpl(ExpenseRepository expenseRepository, UserRepository userRepository, CategoryRepository categoryRepository, ExpenseMapper expenseMapper, AlertRepository alertRepository, BalanceService balanceService, RestTemplate restTemplate) {
        this.expenseRepository = expenseRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.expenseMapper = expenseMapper;
        this.alertRepository = alertRepository;
        this.balanceService = balanceService;
        this.restTemplate = restTemplate;
    }


    @Override
    public ExpenseDto createNewExpense(ExpenseDto newExpense) {
        // add creation date of expense if creation date is empty
        if (newExpense.getDate() == null) {
            LocalDateTime currentDateTime = LocalDateTime.now();
            newExpense.setDate(DateUtility.localDateTimeToString(currentDateTime));
        }
        Category category = categoryRepository.findById(newExpense.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("Not found category with id = " + newExpense.getCategoryId()));
        checkAndSendAlert(category);
        Expense generatedExpense = saveNewExpense(newExpense, category);
        balanceService.addExpense(generatedExpense.getAmount());
        return expenseMapper.toDto(generatedExpense);
    }


    @Override
    public ExpenseDto updateExpense(ExpenseDto expense) {
        Category category = categoryRepository.findById(expense.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("Not found Category related to this expense = " + expense.getCategoryId()));
        Expense existenceExpense = expenseRepository.findById(expense.getId()).orElseThrow(() -> new ResourceNotFoundException("Not found expense with id = " + expense.getId()));
        return updateExistenceExpense(existenceExpense, expense, category);
    }

    @Override
    public void deleteExpense(Long id) {
        expenseRepository.deleteById(id);

    }

    @Override
    public List<ExpenseDto> getExpenses(GetExpenseRequest getExpenseRequest) {
        if (getExpenseRequest.getCategoryId() != null) {
            Category category = categoryRepository.findById(getExpenseRequest.getCategoryId()).orElseThrow(EntityNotFoundException::new);
            List<Expense> expenseList = expenseRepository.findAllByDateBetweenAndCategoryAndUser(getExpenseRequest.getStartDate(), getExpenseRequest.getEndDate(), category, getUser());
            return expenseMapper.toDto(expenseList);
        }
        List<Expense> allByDateBetween = expenseRepository.findAllByDateBetweenAndUser(getExpenseRequest.getStartDate(), getExpenseRequest.getEndDate(), getUser());
        return expenseMapper.toDto(allByDateBetween);
    }


    private User getUser() {
        String username = SecurityUtils.getCurrentUser();
        return userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("USER_NOT_FOUND_EXCEPTION"));
    }

    private Expense saveNewExpense(ExpenseDto newExpense, Category category) {

        Expense expenseEntity = expenseMapper.toEntity(newExpense);
        expenseEntity.setCategory(category);
        expenseEntity.setUser(getUser());
        return expenseRepository.save(expenseEntity);
    }

    private ExpenseDto updateExistenceExpense(Expense existenceExpense, ExpenseDto expense, Category category) {
        checkUpdateBalance(existenceExpense, expense);
        existenceExpense.setAmount(expense.getAmount());
        existenceExpense.setDescription(expense.getDescription());
        existenceExpense.setCategory(category);
        existenceExpense.setDate(DateUtility.stringToLocalDateTime(expense.getDate()));
        return expenseMapper.toDto(expenseRepository.save(existenceExpense));
    }


    private void checkUpdateBalance(Expense existenceExpense, ExpenseDto expense) {
        if (!existenceExpense.getAmount().equals(expense.getAmount())) {
            balanceService.addExpense(expense.getAmount() - existenceExpense.getAmount());
        }
    }

    private void checkAndSendAlert(Category category) {

        User user = getUser();
        Optional<Alert> alertByCategory = alertRepository.findAllByUserAndCategory(user, category);

        if (alertByCategory.isPresent()) {
            Alert alert = alertByCategory.get();
            Double sumOfExpenses = expenseRepository.sumOfExpense(alert.getStartTime(), alert.getEndTime(), category);
            if (alert.getBalance() <= sumOfExpenses) {
                AlertMessage alertMessage = new AlertMessage();
                alertMessage.setMessage(alert.getDescription());
                restTemplate.postForEntity(urlPath, alertMessage, AlertMessage.class);
            }
        }
    }


}
