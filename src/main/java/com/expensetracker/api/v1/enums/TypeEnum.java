package com.expensetracker.api.v1.enums;

public enum TypeEnum {

    EXPENSE("expense"),
    INCOME("income");

    private final String type;

    TypeEnum(String type){
        this.type = type;
    }

    public static TypeEnum valueOfLabel(String type) {
        for (TypeEnum e : values()) {
            if (e.type.equals(type)) {
                return e;
            }
        }
        return null;
    }
}
