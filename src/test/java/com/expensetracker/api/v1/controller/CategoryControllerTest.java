package com.expensetracker.api.v1.controller;

import com.expensetracker.api.v1.domain.CategoryDto;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.service.CategoryService;
import com.expensetracker.api.v1.utils.Utility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
class CategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CategoryService categoryServiceMock;

    CategoryDto category;

    @BeforeEach
    public void init(){
        category = CategoryDto.builder().name("car").description("All expenses for car.").build();
    }

    @Test
    @WithMockUser
    void given_newCategory_when_createCategory_then_createAndSaveNewCategory_status201() throws Exception {

        //Given
        //newCategory = category
        CategoryDto generatedCategory = CategoryDto.builder().id(1L).name("car").description("All expenses for car.").build();

        //When
        Mockito.when(categoryServiceMock.createCategory(category)).thenReturn(generatedCategory);

        //Then
        String url = "/api/v1/category/create";
        mockMvc.perform(
                        post(url).content(Utility.asJsonString(category))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.name").value(generatedCategory.getName()))
                .andExpect(jsonPath("$.description").value(generatedCategory.getDescription()));
    }

    @Test
    @WithMockUser
    void given_nothing_when_getCategories_then_listOfCategories_status200() throws Exception {

        List<CategoryDto> categories = new ArrayList<>();
        categories.add(category);

        //When
        Mockito.when(categoryServiceMock.getCategories()).thenReturn(categories);

        //Then
        String url = "/api/v1/category/get";
        mockMvc.perform(
                 get(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$[0].name").value(categories.get(0).getName()))
                .andExpect(jsonPath("$[0].description").value(categories.get(0).getDescription()));
    }

    @Test
    @WithMockUser
    void given_nothing_when_getCategories_then_throwException() throws Exception {

        //given
        Long categoryId = 1L;

        //When
        Mockito.when(categoryServiceMock.getCategory(categoryId)).thenThrow(new ResourceNotFoundException("Any category does not exist."));

        //Then
        String url = "/api/v1/category/get/1";
        mockMvc.perform(
                 get(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("@.message").value("Any category does not exist."));
    }

    @Test
    @WithMockUser
    void given_id_when_getCategory_then_category_status200() throws Exception {

        //given
        Long categoryId = 12L;

        //When
        Mockito.when(categoryServiceMock.getCategory(any(Long.class))).thenReturn(category);

        //Then
        String url = "/api/v1/category/get/12";
        mockMvc.perform(
                 get(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.id").value(category.getId()))
                .andExpect(jsonPath("$.name").value(category.getName()))
                .andExpect(jsonPath("$.description").value(category.getDescription()));
    }

    @Test
    @WithMockUser
    void given_id_when_getCategory_then_throwException() throws Exception {

        //given
        Long categoryId = 12L;

        //When
        Mockito.when(categoryServiceMock.getCategory(any(Long.class)))
                .thenThrow(new ResourceNotFoundException("Not found Category with id = " + categoryId));

        //Then
        String url = "/api/v1/category/get/12";
        mockMvc.perform(
                        get(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("@.message").value("Not found Category with id = " + categoryId));

    }


    @Test
    @WithMockUser
    void given_category_when_updateCategory_then_updatedCategory() throws Exception {

        //given
        CategoryDto newCategory = CategoryDto.builder().id(12L).name("transportation").description("this is for expenses of transportation").build();

        //When
        Mockito.when(categoryServiceMock.updateCategory((newCategory))).thenReturn(newCategory);

        //Then
        String url = "/api/v1/category/update";
        mockMvc.perform(put(url)
                        .content(Utility.asJsonString(newCategory))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value(newCategory.getName()))
                .andExpect(jsonPath("$.description").value(newCategory.getDescription()));

    }

    @Test
    @WithMockUser
    void given_newCategoryForUpdate_when_updateCategory_then_throwException() throws Exception {

        //given
        CategoryDto newCategory = CategoryDto.builder().id(12L).name("transportation").description("this is for expenses of transportation").build();

        //When
        Mockito.when(categoryServiceMock.updateCategory((newCategory))).thenThrow(new ResourceNotFoundException("Not found Category with id = " +newCategory.getId()));

        //Then
        String url = "/api/v1/category/update";
        mockMvc.perform(put(url)
                        .content(Utility.asJsonString(newCategory))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("@.message").value("Not found Category with id = " +newCategory.getId()));

    }


    @Test
    @WithMockUser
    void given_categoryId_when_deleteCategory_then_status_no_content() throws Exception {

        //given
        Long categoryId = 12L;

        //When
        willDoNothing().given(categoryServiceMock).deleteCategory(categoryId);

        //Then
        String url = "/api/v1/category/delete/{categoryId}";
        mockMvc.perform(delete(url,categoryId))
                .andExpect(status().isNoContent());
    }
}