package com.expensetracker.api.v1.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class CategoryDto {

    private Long id;
    @NotNull
    @NotBlank
    private String name;
    private String description;



}
