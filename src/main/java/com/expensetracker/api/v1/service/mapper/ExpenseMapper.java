package com.expensetracker.api.v1.service.mapper;

import com.expensetracker.api.v1.entity.Expense;
import com.expensetracker.api.v1.domain.ExpenseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ExpenseMapper extends EntityMapper<ExpenseDto, Expense> {

    @Mapping(target = "id", source = "expense.id")
    @Mapping(target = "amount", source = "expense.amount")
    @Mapping(target = "date", source = "expense.date", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(target = "description", source = "expense.description")
    @Mapping(target = "categoryId", expression = "java(Long.valueOf(expense.getCategory().getId()))")
    ExpenseDto toDto(Expense expense);
    List<ExpenseDto> toDto(List<Expense> expense);


    @Mapping(target = "id", source = "dto.id")
    @Mapping(target = "amount", source = "dto.amount")
    @Mapping(target = "date", source = "dto.date", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(target = "description", source = "dto.description")
    Expense toEntity(ExpenseDto dto);
    List<Expense> toEntity(List<ExpenseDto> dto);
}
