package com.expensetracker.api.v1.service.report;


import com.expensetracker.api.v1.enums.TypeEnum;

import java.util.Date;

public interface Type {

    Object getData(Date start, Date end);
    TypeEnum getStrategyReport();


}
