package com.expensetracker.api.v1.service.mapper;


import com.expensetracker.api.v1.domain.BalanceDto;
import com.expensetracker.api.v1.entity.Balance;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BalanceMapper extends EntityMapper<BalanceDto, Balance> {


    @Mapping(target = "id", source = "balance.id")
    @Mapping(target = "totalExpense", source = "balance.totalExpense")
    @Mapping(target = "totalIncome", source = "balance.totalIncome")
    @Mapping(target = "balance", source = "balance.balance")
    BalanceDto toDto(Balance balance);

    List<BalanceDto> toDto(List<Balance> balance);


    @Mapping(target = "id", source = "balanceDto.id")
    @Mapping(target = "totalExpense", source = "balanceDto.totalExpense")
    @Mapping(target = "totalIncome", source = "balanceDto.totalIncome")
    @Mapping(target = "balance", source = "balanceDto.balance")
    Balance toEntity(BalanceDto balanceDto);

    List<Balance> toEntity(List<BalanceDto> balanceDto);
}
