package com.expensetracker.api.v1.utils;

public class Constants {
    public static final String PATTERN_DATE = "yyyy-MM-dd HH:mm:ss";
}
