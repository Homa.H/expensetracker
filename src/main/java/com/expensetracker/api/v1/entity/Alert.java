package com.expensetracker.api.v1.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Alert implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated product ID")
    private Long id;

    @ApiModelProperty(notes = "The Alert label")
    private String label;

    @ApiModelProperty(notes = "The Alert Description")
    private String description;

    @ApiModelProperty(notes = "The Balance Of Max Expense")
    private Double balance;

    @ApiModelProperty(notes = "The Start Time For Computing Of Alert")
    private LocalDateTime startTime;

    @ApiModelProperty(notes = "The end Time For Computing Of Alert")
    private LocalDateTime endTime;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    public Long getId() {
        return id;
    }

}
