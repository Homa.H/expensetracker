package com.expensetracker.api.v1.controller;

import com.expensetracker.api.v1.domain.BalanceDto;
import com.expensetracker.api.v1.service.BalanceService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class BalanceControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    BalanceService balanceServiceMock;
    @Test
    @WithMockUser
    void given_nothing_when_getBalance_then_returnBalanceObjectPerUser() throws Exception {

        BalanceDto balance = BalanceDto.builder().id(12L).totalExpense(2000.0).totalIncome(4000.0).balance(2000.0).build();
        //given

        //when
        Mockito.when(balanceServiceMock.getBalance()).thenReturn(balance);

        //then
        String url = "/api/v1/balance/get";
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(balance.getId()))
                .andExpect(jsonPath("$.totalExpense").value(balance.getTotalExpense()))
                .andExpect(jsonPath("$.totalIncome").value(balance.getTotalIncome()));

    }
}