package com.expensetracker.api.v1.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Balance implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "totalExpense")
    private Double totalExpense;

    @Column(name = "totalIncome")
    private Double totalIncome;

    @Column(name = "balance")
    private Double balance;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

}
