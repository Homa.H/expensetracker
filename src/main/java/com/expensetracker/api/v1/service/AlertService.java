package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.AlertDto;

import java.util.List;

public interface AlertService {

    AlertDto createAlert(AlertDto newAlert);
    AlertDto updateAlert(AlertDto alert);
    void deleteAlert(Long id);
    List<AlertDto> getAlerts();
    AlertDto getAlert(Long categoryId);

}
