package com.expensetracker.api.v1.domain;

import lombok.Data;

import java.io.Serializable;
@Data
public class TokenResponse implements Serializable {
    private final String token;


}