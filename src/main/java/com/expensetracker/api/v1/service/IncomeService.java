package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.request.GetIncomeRequest;
import com.expensetracker.api.v1.domain.IncomeDto;

import java.util.List;

public interface IncomeService {

    IncomeDto createIncome(IncomeDto newIncome);
    IncomeDto updateIncome(IncomeDto income);
    void deleteIncome(Long incomeId);
    List<IncomeDto> getIncomes(GetIncomeRequest getIncomeRequest);
}
