package com.expensetracker.api.v1.controller;


import com.expensetracker.api.v1.domain.CategoryDto;
import com.expensetracker.api.v1.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Level;


@RestController
@RequestMapping("api/v1/category")
@CrossOrigin
@Api(value = "Expense Tracker API. Set of endpoints for Category Operation for Expenses tracker.")
public class CategoryController {

    private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    private final CategoryService categoryService;

    CategoryController(CategoryService categoryService){
        this.categoryService = categoryService;
    }

    @PostMapping("/create")
    @ApiOperation(value = "Endpoint for create  Category. It returns a category generated object.",
            response = CategoryDto.class, httpMethod = "POST")
    ResponseEntity<CategoryDto> createCategory(@RequestBody @Valid CategoryDto newCategory) {
        logger.info(String.format("This is new Category : %s",newCategory.getName()));
        CategoryDto generatedCategory = categoryService.createCategory(newCategory);
        return new ResponseEntity<>(generatedCategory, HttpStatus.CREATED);

    }

    @GetMapping("/get")
    @ApiOperation(value = "Endpoint for get list of Category. It returns a list of Category object based on user.",
            response = CategoryDto.class, httpMethod = "GET")
    ResponseEntity<List<CategoryDto>> getCategories(){
        return new ResponseEntity<>(categoryService.getCategories(),HttpStatus.OK);
    }


    @GetMapping("/get/{categoryId}")
    @ApiOperation(value = "Endpoint for Get  Category. It returns a Category object based on category name.",
            response = CategoryDto.class, httpMethod = "GET")
    ResponseEntity<CategoryDto> getCategory(@PathVariable Long categoryId){
        return new ResponseEntity<>(categoryService.getCategory(categoryId),HttpStatus.OK);

    }

    @PutMapping("/update")
    @ApiOperation(value = "Endpoint for Edit/Update  Category. It returns a Category object updated.",
            response = CategoryDto.class, httpMethod = "PUT")
    ResponseEntity<CategoryDto> updateCategory(@RequestBody @Valid CategoryDto editedCategory){
        return new ResponseEntity<>(categoryService.updateCategory(editedCategory),HttpStatus.OK);
    }

    @DeleteMapping("/delete/{categoryId}")
    @ApiOperation(value = "Endpoint for Delete Category.",
            response = void.class, httpMethod = "DELETE")
    ResponseEntity<HttpStatus> deleteCategory(@PathVariable Long categoryId) {
        categoryService.deleteCategory(categoryId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
