package com.expensetracker.api.v1.domain;

import com.expensetracker.api.v1.utils.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class IncomeDto {

    private Long id;

    private String label;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.PATTERN_DATE)
    private String date;

    private String description;

    @NotNull
    private Double amount;

    @NotNull
    private Long categoryId;

}
