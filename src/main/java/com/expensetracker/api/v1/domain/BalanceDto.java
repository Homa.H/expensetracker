package com.expensetracker.api.v1.domain;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BalanceDto {

    private Long id;
    private Double totalExpense;
    private Double totalIncome;
    private Double balance;

}
