package com.expensetracker.api.v1.producer;

import com.expensetracker.api.v1.domain.AlertMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.jms.Queue;

@RestController
@RequestMapping("/produce")
public class Producer {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Queue queue;

    @PostMapping("/message")
    public AlertMessage sendMessage(@RequestBody AlertMessage message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String messageAsJson = mapper.writeValueAsString(message);
            jmsTemplate.convertAndSend(queue, messageAsJson);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }
}
