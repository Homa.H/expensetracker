package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.UserDto;
import com.expensetracker.api.v1.domain.UserView;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.service.mapper.UserMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService implements UserDetailsService {


    private final UserRepository userRepository;
    private final PasswordEncoder bcryptEncoder;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, PasswordEncoder bcryptEncoder, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.bcryptEncoder = bcryptEncoder;
        this.userMapper = userMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(()-> new ResourceNotFoundException("USER_NOT_FOUND_EXCEPTION"));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
    }

    public UserView save(UserDto user) {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setEmail(user.getEmail());
        User savedUser = userRepository.save(newUser);
        return userMapper.toDto(savedUser);
    }
}
