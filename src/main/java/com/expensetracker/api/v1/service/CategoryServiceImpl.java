package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.CategoryDto;
import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.repository.CategoryRepository;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.security.SecurityUtils;
import com.expensetracker.api.v1.service.mapper.CategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService{

    private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;
    private final CategoryMapper categoryMapper;

    CategoryServiceImpl(CategoryRepository categoryRepository,
                        UserRepository userRepository,
                        CategoryMapper categoryMapper){
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
        this.userRepository = userRepository;
    }

    @Override
    public CategoryDto createCategory(CategoryDto newCategory) {

        logger.info("create category service!!");
        User user = getUser();
        boolean isCategorySaved = isCategorySaved(user, newCategory);
        if(isCategorySaved){
            throw new ResourceAccessException("Category with name :("+newCategory.getName()+") exists.");
        }
        return (saveNewCategory(newCategory,user));
    }

    @Override
    public List<CategoryDto> getCategories() {
        return categoryMapper.toDto(categoryRepository.findByUser(getUser()));
    }

    @Override
    public CategoryDto getCategory(Long categoryId) {
        Category category = categoryRepository.findByIdAndUser(categoryId,getUser())
                .orElseThrow(() -> new ResourceNotFoundException("Not found Category with id = " + categoryId));
        return categoryMapper.toDto(category);
    }

    @Override
    public CategoryDto updateCategory(CategoryDto category) {
        Category existedCategory = categoryRepository.findById(category.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Not found Category with id = " + category.getId()));
        return updateExistedCategory(existedCategory, category);
    }

    @Override
    public void deleteCategory(Long categoryId) {
        categoryRepository.deleteById(categoryId);
    }

    private boolean isCategorySaved(User user, CategoryDto newCategory){
        Optional<Category> savedCategory = categoryRepository.findByNameAndUser(newCategory.getName(), user);
        return savedCategory.isPresent();
    }

    private CategoryDto saveNewCategory(CategoryDto newCategory,User currentUser){
        Category category = categoryMapper.toEntity(newCategory);
        category.setUser(currentUser);
        Category savedCategory = categoryRepository.save(category);
        return categoryMapper.toDto(savedCategory);
    }

    private User getUser(){
        String username = SecurityUtils.getCurrentUser();
        return userRepository.findByUsername(username).orElseThrow(()-> new ResourceNotFoundException("USER_NOT_FOUND_EXCEPTION"));
    }

    private CategoryDto updateExistedCategory(Category existedCategory, CategoryDto category) {
        existedCategory.setName(category.getName());
        existedCategory.setDescription(category.getDescription());
        return categoryMapper.toDto(categoryRepository.save(existedCategory));
    }


}
