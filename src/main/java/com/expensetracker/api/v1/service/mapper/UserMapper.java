package com.expensetracker.api.v1.service.mapper;

import com.expensetracker.api.v1.domain.UserView;
import com.expensetracker.api.v1.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring")
public interface UserMapper extends EntityMapper<UserView, User> {


    @Mapping(target = "id", source = "user.id")
    @Mapping(target = "username", source = "user.username")
    @Mapping(target = "email", source = "user.email")
    UserView toDto(User user);


}
