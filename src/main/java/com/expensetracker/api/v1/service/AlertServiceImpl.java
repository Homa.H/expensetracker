package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.repository.CategoryRepository;
import com.expensetracker.api.v1.repository.UserRepository;
import com.expensetracker.api.v1.utils.DateUtility;
import com.expensetracker.api.v1.domain.AlertDto;
import com.expensetracker.api.v1.entity.Alert;
import com.expensetracker.api.v1.entity.Category;
import com.expensetracker.api.v1.entity.User;
import com.expensetracker.api.v1.repository.AlertRepository;
import com.expensetracker.api.v1.security.SecurityUtils;
import com.expensetracker.api.v1.service.mapper.AlertMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AlertServiceImpl implements AlertService{

    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final AlertRepository alertRepository;
    private final  AlertMapper alertMapper;
    AlertServiceImpl(UserRepository userRepository,
                 CategoryRepository categoryRepository,
                 AlertRepository alertRepository,
                 AlertMapper alertMapper){
        this.alertRepository = alertRepository;
        this.userRepository = userRepository;
        this.alertMapper = alertMapper;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public AlertDto createAlert(AlertDto newAlert) {
        Category category = categoryRepository.findById(newAlert.getCategoryId()).orElseThrow(()->            new ResourceNotFoundException("Not found category with id = " +newAlert.getCategoryId()));
        return saveNewAlert(newAlert,category);
    }

    @Override
    public AlertDto updateAlert(AlertDto alert) {
        Alert alertById = alertRepository.findById(alert.getId())
                .map(updatedAlert -> {
                    updatedAlert.setLabel(alert.getLabel());
                    updatedAlert.setDescription(alert.getDescription());
                    updatedAlert.setBalance(alert.getBalance());
                    updatedAlert.setStartTime(DateUtility.stringToLocalDateTime(alert.getStartTime()));
                    updatedAlert.setEndTime(DateUtility.stringToLocalDateTime(alert.getEndTime()));
                    return alertRepository.save(updatedAlert);
                }).orElseThrow(() -> new ResourceNotFoundException("Not found Alert with id = " + alert.getId()));
    return alertMapper.toDto(alertById);
    }

    @Override
    public void deleteAlert(Long alertId) {
        alertRepository.deleteById(alertId);
    }

    @Override
    public List<AlertDto> getAlerts() {
        User user = getUser();
        List<Alert> alerts = alertRepository.findAllByUser(user);
        return alertMapper.toDto(alerts);
    }

    @Override
    public AlertDto getAlert(Long categoryId) {

        User user = getUser();
        Optional<Category> category = categoryRepository.findById(categoryId);
        if(!category.isPresent()){
           throw new ResourceNotFoundException("Not found Category with id = " + categoryId);
        }
        Alert alert = alertRepository.findAllByUserAndCategory(user,category.get())
                .orElseThrow(() -> new ResourceNotFoundException("Not found Alert with category id = " + categoryId));
        return alertMapper.toDto(alert);

    }
    private User getUser(){
        return userRepository.findByUsername(SecurityUtils.getCurrentUser()).orElseThrow(()-> new ResourceNotFoundException("USER_NOT_FOUND_EXCEPTION"));
    }
    private AlertDto saveNewAlert(AlertDto newAlert, Category category) {
        User user = getUser();
        Alert alertEntity = alertMapper.toEntity(newAlert);
        alertEntity.setCategory(category);
        alertEntity.setUser(user);
        Alert saveAlert = alertRepository.save(alertEntity);
        return alertMapper.toDto(saveAlert);
    }


}
