package com.expensetracker.api.v1.service.mapper;

import com.expensetracker.api.v1.domain.AlertDto;
import com.expensetracker.api.v1.entity.Alert;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AlertMapper extends EntityMapper<AlertDto, Alert> {


    @Mapping(target = "id", source = "alert.id")
    @Mapping(target = "label", source = "alert.label")
    @Mapping(target = "description", source = "alert.description")
    @Mapping(target = "balance", source = "alert.balance")
    @Mapping(target = "startTime", source = "alert.startTime")
    @Mapping(target = "endTime", source = "alert.endTime")
    @Mapping(target = "categoryId", expression = "java(Long.valueOf(alert.getCategory().getId()))")
    AlertDto toDto(Alert alert);
    List<AlertDto> toDto(List<Alert> alerts);


    @Mapping(target = "id", source = "model.id")
    @Mapping(target = "label", source = "model.label")
    @Mapping(target = "description", source = "model.description")
    @Mapping(target = "balance", source = "model.balance")
    @Mapping(target = "startTime", source = "model.startTime")
    @Mapping(target = "endTime", source = "model.endTime")
    Alert toEntity(AlertDto model);
    List<Alert> toEntity(List<AlertDto> model);
}
