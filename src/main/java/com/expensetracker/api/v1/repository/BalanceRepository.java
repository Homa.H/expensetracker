package com.expensetracker.api.v1.repository;

import com.expensetracker.api.v1.entity.Balance;
import com.expensetracker.api.v1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BalanceRepository extends JpaRepository<Balance,Long> {

    Optional<Balance> findBalanceByUser(User user);
}
