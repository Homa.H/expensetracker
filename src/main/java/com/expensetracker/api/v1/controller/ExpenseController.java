package com.expensetracker.api.v1.controller;


import com.expensetracker.api.v1.domain.ExpenseDto;
import com.expensetracker.api.v1.domain.request.GetExpenseRequest;
import com.expensetracker.api.v1.service.ExpenseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;



@RestController
@RequestMapping("api/v1/expense")
@CrossOrigin
@Api(value = "Expense Tracker API. Set of endpoints for expense Operation(create/update/get/delete) for Expenses tracker.")
public class ExpenseController {

    private static final Logger logger = LoggerFactory.getLogger(ExpenseController.class);

    private final ExpenseService expenseService;
    ExpenseController(ExpenseService expenseService){
        this.expenseService = expenseService;
    }

    @PostMapping ("/create")
    @ApiOperation(value = "Endpoint for create  Expense. It returns a Expense object.",
            response = ExpenseDto.class, httpMethod = "POST")
    ResponseEntity<ExpenseDto> createExpense(@RequestBody @Valid ExpenseDto newExpense){
        logger.info("create new expense.");
        return new ResponseEntity<>(expenseService.createNewExpense(newExpense),HttpStatus.CREATED);
    }

    @PostMapping("/get")
    @ApiOperation(value = "Endpoint for Get Expense. It returns a Expense object based on time duration and/or category id.",
            response = ExpenseDto.class, httpMethod = "POST")
    ResponseEntity<List<ExpenseDto>> getExpenses(@RequestBody GetExpenseRequest getExpenseRequest){
        return new ResponseEntity<>(expenseService.getExpenses(getExpenseRequest),HttpStatus.OK);
    }
    @PutMapping("/update")
    @ApiOperation(value = "Endpoint for Edit/Update Expense. It returns a Expense object updated.",
            response = ExpenseDto.class, httpMethod = "PUT")
    ResponseEntity<ExpenseDto> updateExpense(@RequestBody @Valid ExpenseDto expense){
        return new ResponseEntity<>(expenseService.updateExpense(expense),HttpStatus.OK);
    }

    @DeleteMapping("/delete/{expenseId}")
    @ApiOperation(value = "Endpoint for Delete Expense.",
            response = HttpStatus.class, httpMethod = "DELETE")
    ResponseEntity<HttpStatus> deleteExpense(@PathVariable Long expenseId){
        expenseService.deleteExpense(expenseId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
