package com.expensetracker.api.v1.domain;

import lombok.Data;

@Data
public class UserView {

    private Long id;
    private String username;
    private String email;

}
