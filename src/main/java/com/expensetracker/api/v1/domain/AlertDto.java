package com.expensetracker.api.v1.domain;

import com.expensetracker.api.v1.utils.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;



@Data
@Builder
public class AlertDto {

    private Long id;
    private String label;
    private String description;
    private Double balance;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = Constants.PATTERN_DATE)
    private String startTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = Constants.PATTERN_DATE)
    private String endTime;


    private Long categoryId;

}
