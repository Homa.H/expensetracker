package com.expensetracker.api.v1.service.mapper;

import com.expensetracker.api.v1.domain.CategoryDto;
import com.expensetracker.api.v1.entity.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper extends EntityMapper<CategoryDto, Category>{


    @Mapping(target="id", source="category.id")
    @Mapping(target="name", source="category.name")
    @Mapping(target="description", source="category.description")
    CategoryDto toDto(Category category);
    List<CategoryDto> toDto(List<Category> category);


    @Mapping(target="name", source="model.name")
    @Mapping(target="description", source="model.description")
    Category toEntity(CategoryDto model);
    List<Category> toEntity(List<CategoryDto> model);



}
