package com.expensetracker.api.v1.controller;

import com.expensetracker.api.v1.domain.AlertDto;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.service.AlertService;
import com.expensetracker.api.v1.utils.Constants;
import com.expensetracker.api.v1.utils.Utility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;


import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class  AlertControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AlertService alertServiceMock;
    private AlertDto alert;

    @BeforeEach
    public void init(){
        String start = "2022-03-04 11:30:00";
        String end = "2022-03-05 11:30:00";
        alert = AlertDto.builder().label("car").description("alert for expenses of car.")
                .balance(200000.0).categoryId(1L).startTime(start).endTime(end).build();

    }

    @Test
    @WithMockUser
    void given_newAlert_when_createAlert_then_createAndSaveGeneratedAlertObject() throws Exception {

        //given
        //new alert

        //when
        Mockito.when(alertServiceMock.createAlert(alert)).thenReturn(alert);

        //then
        String url = "/api/v1/alert/create";
        mockMvc.perform(
                        post(url).content(Utility.asJsonString(alert))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.label").value(alert.getLabel()))
                .andExpect(jsonPath("$.description").value(alert.getDescription()));
    }

    @Test
    @WithMockUser
    void given_categoryId_when_getAlert_then_alertObject() throws Exception {
        //given
        Long categoryId = 1L;

        //when
        Mockito.when(alertServiceMock.getAlert(categoryId)).thenReturn(alert);

        //then
        String url = "/api/v1/alert/get/1";
        mockMvc.perform(
                        get(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.label").value(alert.getLabel()))
                .andExpect(jsonPath("$.categoryId").value(alert.getCategoryId()))
                .andExpect(jsonPath("$.description").value(alert.getDescription()));
    }

    @Test
    @WithMockUser
    void given_incorrectCategoryId_when_getAlert_then_throwException() throws Exception {

        //given
        Long categoryId = 1L;

        //when
        Mockito.when(alertServiceMock.getAlert(categoryId)).thenThrow(new ResourceNotFoundException("Not found Category with id = " + categoryId));

        //then
        String url = "/api/v1/alert/get/1";
        mockMvc.perform(
                get(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("@.message").value("Not found Category with id = " + categoryId));
    }

    @Test
    @WithMockUser
    void given_nothing_when_getAlerts_then_listOfAlert() throws Exception {

        //given
        List<AlertDto> alerts = new ArrayList<>();
        alerts.add(alert);

        //when
        Mockito.when(alertServiceMock.getAlerts()).thenReturn(alerts);

        //then
        String url = "/api/v1/alert/get";
        mockMvc.perform(
                get(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].label").value(alerts.get(0).getLabel()))
                .andExpect(jsonPath("$[0].description").value(alerts.get(0).getDescription()))
                .andExpect(jsonPath("$[0].categoryId").value(alerts.get(0).getCategoryId()))
                .andExpect(jsonPath("$[0].balance").value(alerts.get(0).getBalance()));
    }

}