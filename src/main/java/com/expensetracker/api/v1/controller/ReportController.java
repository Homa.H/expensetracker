package com.expensetracker.api.v1.controller;

import com.expensetracker.api.v1.enums.DurationEnum;
import com.expensetracker.api.v1.enums.TypeEnum;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.service.report.Duration;
import com.expensetracker.api.v1.service.report.ReportServiceClass;
import com.expensetracker.api.v1.service.report.Type;
import com.expensetracker.api.v1.service.report.strategy.DurationStrategy;
import com.expensetracker.api.v1.service.report.strategy.TypeStrategy;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/report")
@Api(value = "Expense Tracker API. Set of endpoints for Report Operation(weekly/monthly/daily) for expense and income based on category.")
public class ReportController {

    private static final Logger logger = LoggerFactory.getLogger(ReportController.class);

    private final DurationStrategy durationStrategy;
    private final TypeStrategy typeStrategy;

    ReportController(DurationStrategy durationStrategy,TypeStrategy typeStrategy){
        this.durationStrategy = durationStrategy;
        this.typeStrategy = typeStrategy;
    }

    @GetMapping("/{durationReport}/{typeReport}")
    Object getReport(@PathVariable String durationReport,@PathVariable String typeReport){

        if(durationReport.isEmpty() || typeReport.isEmpty()){
                throw new ResourceNotFoundException("Not found duration time");
        }
        logger.info(String.format("Duration for report is : {0}",durationReport));
        logger.info(String.format("Type for report is : {0}",typeReport));
        DurationEnum durationEnum = DurationEnum.valueOfLabel(durationReport);
        TypeEnum typeEnum = TypeEnum.valueOfLabel(typeReport);
        if (durationEnum ==null || typeEnum == null) {
            throw new ResourceNotFoundException("input (duration or/and type) for getting report is wrong!");
        }
        Duration strategyDuration = durationStrategy.findStrategy(DurationEnum.valueOfLabel(durationReport));
        Type strategyType = typeStrategy.findStrategy(TypeEnum.valueOfLabel(typeReport));
        ReportServiceClass reportServiceClass = new ReportServiceClass(strategyDuration,strategyType);
        return reportServiceClass.getReport();
    }



}
