package com.expensetracker.api.v1.domain.request;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
public class GetExpenseRequest {

    @NotNull
    @NotBlank
    private LocalDateTime startDate;
    @NotNull
    @NotBlank
    private LocalDateTime endDate;
    private Long categoryId;
}
