package com.expensetracker.api.v1.service.report.duration;

import com.expensetracker.api.v1.enums.DurationEnum;
import com.expensetracker.api.v1.service.report.Duration;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class Monthly implements Duration {
    @Override
    public Date[] getDuration() {
        Date currentDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.MONTH,-1);
        Date endTime = calendar.getTime();
        return   new Date[]{currentDate,endTime};
    }

    @Override
    public DurationEnum getStrategyReport() {
        return DurationEnum.MONTHLY;
    }
}
