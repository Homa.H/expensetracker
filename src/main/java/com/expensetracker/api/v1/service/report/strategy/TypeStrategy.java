package com.expensetracker.api.v1.service.report.strategy;

import com.expensetracker.api.v1.service.report.Type;
import com.expensetracker.api.v1.enums.TypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class TypeStrategy {
    private EnumMap<TypeEnum, Type> strategies;

    @Autowired
    public TypeStrategy(Set<Type> strategySet) {
        createStrategy(strategySet);
    }

    public Type findStrategy(TypeEnum strategyName) {
        return strategies.get(strategyName);
    }


    private void createStrategy(Set<Type> strategySet) {
        strategies = new EnumMap<>(TypeEnum.class);
        strategySet.forEach(
                strategy ->strategies.put(strategy.getStrategyReport(), strategy));
    }
}
