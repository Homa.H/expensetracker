package com.expensetracker.api.v1.service;

import com.expensetracker.api.v1.domain.BalanceDto;


public interface BalanceService {
    BalanceDto getBalance();
    BalanceDto addExpense(Double amount);
    BalanceDto addIncome(Double amount);}
