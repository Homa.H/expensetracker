package com.expensetracker.api.v1.controller;

import com.expensetracker.api.v1.domain.ExpenseDto;
import com.expensetracker.api.v1.domain.request.GetExpenseRequest;
import com.expensetracker.api.v1.exception.ResourceNotFoundException;
import com.expensetracker.api.v1.service.ExpenseService;
import com.expensetracker.api.v1.utils.Utility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ExpenseControllerTest {

    @MockBean
    ExpenseService expenseServiceMock;
    @Autowired
    private MockMvc mockMvc;


    String start = "2022-03-04 11:30:00";
    String end = "2022-03-05 11:30:00";
    LocalDateTime startDate ;
    LocalDateTime endDate;

    @BeforeEach
    void init() throws ParseException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        startDate = LocalDateTime.parse(start, formatter);
        endDate = LocalDateTime.parse(end, formatter);
    }

    @Test
    @WithMockUser
    void given_newExpense_when_createExpense_then_saveNewExpense() throws Exception {

        //given
        ExpenseDto newExpense = ExpenseDto.builder().categoryId(12L).label("car_expense")
                .amount(1000.0).date(start).description("this is car expense").build();

        ExpenseDto generatedExpense = ExpenseDto.builder().id(1L).categoryId(12L).label("car_expense")
                .amount(1000.0).date(start).description("this is car expense").build();

        //when
        Mockito.when(expenseServiceMock.createNewExpense(newExpense)).thenReturn(generatedExpense);

        //then
        String url = "/api/v1/expense/create";
        mockMvc.perform(post(url)
                        .content(Utility.asJsonString(newExpense))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.label").value(generatedExpense.getLabel()));

    }

    @Test
    @WithMockUser
    void given_requestIsConsistedOfDateAndCategoryId_when_getExpenses_then_returnListOfExpenses() throws Exception {

        //given
        GetExpenseRequest expenseRequest = GetExpenseRequest.builder().startDate(startDate)
                .endDate(startDate).categoryId(12L).build();
        ExpenseDto expense = ExpenseDto.builder().categoryId(12L).label("car_expense")
                .amount(1000.0).date(start).description("this is car expense").build();
        List<ExpenseDto> expenses = new ArrayList<>();
        expenses.add(expense);

        //when
        Mockito.when(expenseServiceMock.getExpenses(expenseRequest)).thenReturn(expenses);

        //then
        String url = "/api/v1/expense/get";
        mockMvc.perform(post(url)
                        .content(Utility.asJsonString(expenseRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].label").value(expenses.get(0).getLabel()));


    }

    @Test
    @WithMockUser
    void given_editedExpense_when_editExpense_then_returnUpdatedExpenses() throws Exception {

        //given
        ExpenseDto editedExpense = ExpenseDto.builder().id(1L).categoryId(12L).label("car_expense")
                .amount(1000.0).date(start).description("this is car expense").build();

        ExpenseDto updatedExpense = ExpenseDto.builder().id(1L).categoryId(12L).label("car_expense")
                .amount(1000.0).date(start).description("this is car expense").build();

        //when
        Mockito.when(expenseServiceMock.updateExpense(editedExpense)).thenReturn(updatedExpense);

        //then
        String url = "/api/v1/expense/update";
        mockMvc.perform(put(url)
                        .content(Utility.asJsonString(editedExpense))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.label").value(updatedExpense.getLabel()));
    }

    @Test
    @WithMockUser
    void given_editedExpenseWithIncorrectCategoryId_when_editExpense_then_throwException() throws Exception {
        //given
        ExpenseDto editedExpense = ExpenseDto.builder().id(1L).categoryId(12L).label("car_expense")
                .amount(1000.0).date(start).description("this is car expense").build();

        //when
        Mockito.when(expenseServiceMock.updateExpense(editedExpense)).thenThrow(new ResourceNotFoundException("Not found category with id = " +editedExpense.getCategoryId()));

        //then


        String url = "/api/v1/expense/update";
        mockMvc.perform(put(url)
                        .content(Utility.asJsonString(editedExpense))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("@.message").value("Not found category with id = " +editedExpense.getCategoryId()));
    }

    @Test
    @WithMockUser
    void given_expenseId_when_deleteExpense_then_deleteExpense() throws Exception {

        //given
        Long expenseId = 12L;

        //When
        willDoNothing().given(expenseServiceMock).deleteExpense(expenseId);

        //Then
        String url = "/api/v1/expense/delete/{expenseId}";
        mockMvc.perform(delete(url,expenseId))
                .andExpect(status().isNoContent());
    }
}