package com.expensetracker.api.v1.controller;

import com.expensetracker.api.v1.domain.BalanceDto;
import com.expensetracker.api.v1.service.BalanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/balance")
@CrossOrigin
@Api(value = "Expense Tracker API.endpoint for Get Balance Operation for Expenses tracker.")
public class BalanceController {

    private final BalanceService balanceService;
    BalanceController(BalanceService balanceService){
        this.balanceService = balanceService;
    }
    @GetMapping("/get")
    @ApiOperation(value = "Endpoint for Get Balance. It returns a Balance object.",
            response = BalanceDto.class, httpMethod = "GET")
    ResponseEntity<BalanceDto> getBalance(){
        return new ResponseEntity<>(balanceService.getBalance(), HttpStatus.OK);
    }
}
