package com.expensetracker.api.v1.domain;

import com.expensetracker.api.v1.utils.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.sql.Date;
import java.time.LocalDateTime;

@Data
@Builder
public class Report {

    private Long id;
    private String label;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.PATTERN_DATE)
    private String date;
    private String description;
    private Double amount;
    private Long categoryId;
}
